async function getUsers() {
    let url = 'users.json';
    try {
        let res = await fetch(url);
        return await res.json();
    } catch (error) {
        console.log(error);
    }
}

async function renderUsers() {
    let users = await getUsers();
    let html = '';
    users.forEach(user => {
        let htmlSegment = `
                       <div class="col-6"> 
                        <div class="user row">
                           <div class="col"> <img src="${user.url}"></div>
                           <div class="col">
                            <h2>${user.title}</h2>
                            <img src="${user.thumbnailUrl}">
                            </div>
                        </div>
                        </div>`;
        html += htmlSegment;
    });

    let row = document.querySelector('.row');
    row.innerHTML = html;
}

renderUsers();
